# DTNL PHP Gitflow

In general Dept uses a simplified gitflow model where environment branches are equal to an environment (machine). 
Meaning that the acceptance branch is equal to acceptance environment. 
This is setup to allow multiple teams to work on the same project without deploying each others work.

## Creating common branches (simplified) :
In this diagram we can see a simplified overview of the PHP gitflow. The process is as follows:

Note: in the following steps when referring to a/the branch we're referring to a hotfix/feature/release branch.

<ol>
<li>For any new code introduced to the code base branch off of the master branch. This way you will always start your new branch off with code the client is using in production.</li>
<li>Next you add in your code in the branch. Once you finished and tested your code you will want to deploy it for testing.</li>
<li>Merge your branch with the `develop` environment branch. Most `develop` environment branches deploy automatically with our CI/CD. And ask your client to approve the changes. If approved:</li>
<li>Merge your branch with the Acceptance/Staging environment branch. Acceptance/Staging branches will not deploy automatically. Go to your projects deployment plan a create a new release and deploy it to acceptance. If approved:</li>
<li>Merge your branch with the `master` branch and follow the same deployment procedure as Acceptance/Staging to deploy it. Master will NEVER run automatically because that would endanger the production environment.</li>
</ol>

![Drag Racing](Images/Gitflow-1.png)

## Why NEVER merge environment branches? (simplified)

As mentioned before you will always want to refrain from merging environment branches. But why? 
To explain this we will illustrate a real life situation:

You have 2 teams working on this project:

- DTNL Support team
- DTNL Project team <br />

The support team has CALL-0001 running on develop and is waiting for approval by the client.
Meanwhile the project team has received a request for a new release regarding GDPR regulations.

<ol>
<li>The project creates a new branch to work on as explained in chapter 1.</li>
<li>The project team made changes within their release branch and is ready for deploy.</li>
<li>The project team merges the changes to test/develop. The client approves project team changes:</li>
<li>The project team merges Test/Develop to acceptance/staging and is now at fault for deploying a untested hotfix (CALL-0001) to acceptance. The client approves project team changes:</li>
<li>The project team merges Acceptance/Staging to Master. The project team has now effectively deployed an untested change without approval of the client.</li>
</ol>

So as we see here in step 4 and 5 an issue is created where the untested hotfix branch (CALL-0001) is deployed to 
the next environment and eventually ends up on production. Not only will this deploy a change that's untested and in 
danger of creating bugs the support flow now has a ticket which is deployed without their consent. When the support 
developer proceeds with his ticket he will now notice that his change is already deployed and might've actually 
already spawned CALL-0002 as a result.

The only exception to merging environment branches is MASTER. Master can always be merged to ANY branch. 
This is because the code that's in production has already been approved and in theory poses no danger for other 
branches and is never able to bring unwanted tickets along. Please see chapter 3.

![Drag Racing](Images/Gitflow-2.png)

## Merging master to other branches is always allowed

The exception to merging environment branches is the master branch. The master branch can and should always be mergeable 
since it is a representation of the code that lives in production. Meaning that any and all features that are living in 
production have been approved and can be merged back to an implementation since they have been approved. One would never 
run into problems of deploying something that is still waiting for approval because all features on production have 
received approval. See the following example why one would want to merge master back to a hotfix.

<ol>
<li>First we'll create a hotfix branch for a ticket. Lets say CALL-0002.
<li>Meanwhile the project team will start a release related to the GDPR regulation.
<li>The project team starts developing and adding code to their branch.
<li>The support team will do the same.
<li>Now the project team has received all required approvals as mentioned in chapter 1 and merges to master. 
The project team is now adding code to the form elements.
<li>The support developer made notice of the change within the form elements and needs to retest his fix with the 
additional code but his branch is behind in master. The support developer merges the approved changes into his hotfix.
<li>The developer adds additional code to be able to support the changes made by the GDPR release from the project team. 
And as mentioned in Chapter 1 has received all approvals for deployment. The support developer merges to master for deploy.
</ol>

![Drag Racing](Images/Gitflow-3.png)