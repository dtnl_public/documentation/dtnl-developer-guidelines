# Cronjobs
A cronjob is a task run every X amount of time. We usually use Cronjobs for clearing cache, running drush commands and executing our own custom scripts.
in this guide we will explain how to:
* access an exisiting cronjob
* editing a cronjob
* debugging a cronjob

![flow](crontab.png)

### access an exisiting cronjob
* First ssh into the server you want to change the cronjob in.
* To see the current cron tab run:
    - `sudo crontab -e -u www-data`

        -e means edit
    
        -u the user to run the cronjob from (every user can have its own crontab)
        the user can sometimes be different. EXAMPLE: for bastion it is bamboo.
* this will open the crontab and here you can see the current scripts that are being executed.
you can also see when this cronjob will be executed.

### editing
Follow the steps for accessing a cronjob first.

You want to edit a crontab is two cases:
* adding a script to the crontab
* changing the time a crontab needs to run.

#### adding a script
In the screenshot above you can see how a cronjob is structured. 
Following this image will make adding a script easy!

A cronjob exists out of:
* a time
* directory of the script
* the script or commands
* place to write the logs to 

Fill these in correctly and your script will be run on the next cronjob time.

#### editing the time
The first part of a cronjob is the time when it needs to fire.
To fill this in or check this correctly use: https://crontab.guru/

### debugging
Follow the steps for accessing a cronjob first.

To debug a crontab basically means running every script that the crontab is running one by one.
If a script gives an error go to that file and debug it like you normally would.
When fixed run the script again, if it works push the changes.

The next time the crontab will run it will work again.
