# Content types
A website can contain a variety of pages: Contact, agenda, news, vacancies, information.. etc.
And for these types of pages we *can* create content types in Drupal. 
See what I did there? You *CAN* create content types for all these sort of pages but this might not always be the right solution.
In this guide we will explain when is the right moment to make a content type and when not.

Paragraphs and content types go hand in hand so make sure to read up on [paragraphs](https://www.paragraphslink.com)

## What content type do I make for a paragraph page?
Now we use paragraphs we can make our pages really dynamic.
If we want to make a *dynamic page* we will create a content type called either:
* Dynamic page
* Paragraph page

This content type can then contain multiple paragraphs.
Because of paragraphs we dont need a content type for every type of page,
instead now we can create a *dynamic page* for the customer.

#### Example
We've created a *dynamic page* content type.
In this content type we've set three fields:
* Page title
* Page sub title
* paragraphs

we want to create 3 page's for the customer: info page, about us, product info

these 3 pages are quite identical so we can use the *dynamic page* content type for all of them.
We can do this because we can select different paragraphs for each of these pages.

Making the paragraphs dynamic and reuseable makes this possible.

## When do I make a new content type?
You make a new content type when either the *dynamic page* content type isn't made yet because you will almost always need that one.

You make a new content type when you're making a page that has really specific information on it and this information isn't going to be reused.
This will almost never be the case because even for a contact page it would be better to:
* make a paragraph block (contact)
* make a page with content type: dynamic page
* use the contact paragraph block here.

this way if you ever want the contact block on another page you can do that!
Making a content type for the contact page would mean most data on it is static.
