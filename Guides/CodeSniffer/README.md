# PHP_CodeSniffer

PHP_CodeSniffer is used to ensure a coding style. 
If you want to read more when and where coding styles are useful 
read the stackoverflow answer by the author[here](https://stackoverflow.com/questions/982333/how-useful-is-php-codesniffer-code-standards-enforcement-in-general).



## Installation
1. Run `composer require squizlabs/php_codesniffer --dev` in your repository.
2. When installing the repository for the first time using composer it will ask you to create defaults for you. Answer Yes.
It will configure the repository and add the correct .gitignore entries.
    ```
       -  WARNING  squizlabs/php_codesniffer (>=3.0): From github.com/symfony/recipes-contrib:master
         The recipe for this package comes from the "contrib" repository, which is open to community contributions.
         Review the recipe at https://github.com/symfony/recipes-contrib/tree/master/squizlabs/php_codesniffer/3.0
     
         Do you want to execute this recipe?
         [y] Yes
         [n] No
         [a] Yes for all packages, only for the current installation session
         [p] Yes permanently, never ask again for this project
         (defaults to n): 
    ```
3. copy [phpcs-psr2.xml](phpcs-psr2.xml) to `phpcs.xml` on the same level as `composer.json` and configure the paths:
    ```XML
    <!-- Define the sources to scan here -->
    <file>./src</file>
    ```
4. Run `phpcs` within your command line to check for violations.
    ```bash
    FILE: src/Repository/SiteRepository.php
    --------------------------------------------------------------------------------------------------------------------
    FOUND 3 ERRORS AFFECTING 3 LINES
    --------------------------------------------------------------------------------------------------------------------
     18 | ERROR | [x] Opening brace should be on the same line as the declaration
        |       |     (Generic.Functions.OpeningFunctionBraceKernighanRitchie.BraceOnNewLine)
     41 | ERROR | [x] Opening brace should be on the same line as the declaration
        |       |     (Generic.Functions.OpeningFunctionBraceKernighanRitchie.BraceOnNewLine)
     53 | ERROR | [x] Opening brace should be on the same line as the declaration
        |       |     (Generic.Functions.OpeningFunctionBraceKernighanRitchie.BraceOnNewLine)
    --------------------------------------------------------------------------------------------------------------------
    PHPCBF CAN FIX THE 3 MARKED SNIFF VIOLATIONS AUTOMATICALLY
    --------------------------------------------------------------------------------------------------------------------
    ```
    
5. Run `phpcbf` to auto-fix violations.
    ```bash
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF.FFFFFFFFFFFFFFFFFFFF 60 / 68 (88%)
    FFFFFFFF                                                     68 / 68 (100%)


    PHPCBF RESULT SUMMARY
    ----------------------------------------------------------------------------------------
    FILE                                                                    FIXED  REMAINING
    ----------------------------------------------------------------------------------------
    src/Repository/SiteRepository.php                                       3      0
    ----------------------------------------------------------------------------------------
    A TOTAL OF 334 ERRORS WERE FIXED IN 67 FILES
    ----------------------------------------------------------------------------------------    
    ```
    
6. Run `phpcs` to ensure no errors were found.

    ```bash
    ............................................................ 60 / 68 (88%)
    ........                                                     68 / 68 (100%)
    
    
    Time: 913ms; Memory: 4MB
    ```