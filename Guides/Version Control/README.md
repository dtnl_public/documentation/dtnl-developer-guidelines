Version Control
===============
To understand Dept version control there are several subjects:

* Branch types
* Commit messages
* Types of changes
* Flow of branches (See `0 - Gitflow`)


## Branch types

DTNL PHP gitflow uses the following branch prefixes:

Hotfixes: `hotfix/`<br />
Hotfix branches are branches commonly used by the Service Desk. These are bugfixes that do not wait for releases.
These bugfixes can be merged directly with environment branches.

Features: `feature/`<br />
A feature branch, commonly used in a sprint. Multiple features branches will likely be merged to a release branch. To
ensure a future release.

Bugfix: `bugfix/`<br />
A bugfix branch, commonly used to fix bugs in feature branches related to a release. These branches are NOT ALLOWED to
be merged into environment branches without the use of a feature branch.

Release: `release/`<br />
A release branch, commonly used when starting a new release / sprint. All work with this release will be merged towards
this branch.

security: `security/`<br />
A security branch, commonly used when Drupal releases security updates which need to be release as soon as possible.


## Commit messages
Note that commit messages do not end with a dot. Please prefix all you commits with a ticket number:

* For a support ticket this should look like: <br />
    ```CALL-12345 Fix things```
* For Jira stick to the ticket naming convention: <br />
    ```PRJ-123 Fix things```


## Types of changes
Depending on your change prefix your commit message with the following keywords. 
Please note that commit messages always use antonyms. 

* `Added` for new features.<br />
* `Changed` for changes in existing functionality.<br />
* `Deprecated` for soon-to-be removed features.<br />
* `Removed` for now removed features.<br />
* `Fixed` for any bug fixes.<br />
* `Security` in case of vulnerabilities.<br />