# Code styles
In our Drupal projects we use a PHP code sniffer to make sure our code style is perfect.
Using this forces a default code style over all our projects, this make our projects more
readable and cleaner.

### How to use a code sniffer
All projects based on our Drupal skeleton have a code sniffer automatically installed.
To use the code sniffer in these projects all you have to do is try to commit code.
If the code you're trying to commit is faulty the sniffer will show you how to fix it.

*However..*
In our older projects a code sniffer isn't installed by default.
To use a code sniffer follow the guide [here](https://github.com/squizlabs/PHP_CodeSniffer).

This guide is really clear on how to install the code sniffer and how to use it.
