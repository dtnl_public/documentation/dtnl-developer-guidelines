# Deploying projects

Before you deploy a project it's good to know a few details.
In this guide you can find information about:

* which platform we use for our deploys
* what you need to do before a deploy
* how you can adjust deployment scripts.
* and more info regarding deploying your project!
