# Updating Drupal 8/9
Drupal will keep updating their CMS and fix security issues. If you're reading this there is probably a security update available!
Before updating make sure you have the latest version of the project.

If you want to check the patch notes you can find them here: https://www.drupal.org/project/drupal/releases/

1. make a clean checkout from master
2. call the branch: security/TICKETNUMBER_drupal_security_update_DRUPALVERSIONNUMBER
3. run a drush cr (cache clear) and clear your stash
4. check which php version the project is working on ( on master )
5. switch to this PHP version before proceeding (else composer will break on master)
6. change the core version to the security update version in composer.json
7. run composer update
8. run drush cr, drush up:db and drush csim -y to check if these commands work. If yes: ALL OKAY!
9. commit changes and push


### common errors

* Version mismatch: If you run composer update while on a different version of PHP then other environments it will create version mismatching. For example: if you locally run composer update while on php 7.2 then it will install packages made for PHP 7.2. If the server is PHP 7.4 these packages wont work.
