PHP Developer Guidelines
========================

These guidelines outline the rules for the development of PHP based projects
for DTNL. Please follow these rules to assure the code created to be consistent.

Guides
--------

* [Version Control](Guides/Version\ Control/README.md)
* [CodeSniffer](Guides/CodeSniffer/README.md)

