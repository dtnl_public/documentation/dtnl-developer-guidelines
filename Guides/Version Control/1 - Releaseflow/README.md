# Release flow
When making features for a sprint we work a bit differently then we do when fixing bugs.
To make clear how we do the releases we wrote this guide.
In this guide you will find a release flow and some common tips to make your release as smooth as possible.

## git release flow
When we work on a release we always make a release branch from master.
before we make this branch you should pull master first to get the latest changes.

Before you checkout to your release branch make sure you get the latest database from the PROD server too.
This ensures your code and your database is exactly the same.

*Before checking out run:*

~ drush csim -y
~ drush cr 

*Then you can safely run:*

~ git checkout -b release/sprint_name

### Flow diagram
![flow](Screenshot_2021-02-25_at_16.20.24.png)

You can always merge your feature branches to the release branch but never the release branch to test, acceptance, master.
The ONLY exception for this is when the release is ready.

If your release is ready you can follow the normal OTAP flow.

## Tips / common mistakes
There are a few tips and common mistakes which you might learn from!

### make sure your database is always in sync
When you are adding new paragraph types, content types, etc. Then make a new page from a content type or add a paragraph to a page and then switch to another branch you will get conflicts.

This happens because the configs you just made for the paragraphs and content types exist in your branch but not in the branch youre checking out to.
Drupal then will throw errors like: Content type could not be found.

That's why it is important to keep your configs and database in sync.

### dont forget to import configs when switching branches
Else you will get issues like explained above. To keep your imports in sync always:

* Lou kan je hier schrijven waarom dit moet en hoe. ik ben een beetje kwijt hoe dit moet.

### always make sure your feature branches have the latest version of the release branch
If you're working on various features and you push these to the feature branch it is important to also merge the release branch into your feature branch.
If there are conflicts it is better to solve them in your feature branch than in your release branch. 
