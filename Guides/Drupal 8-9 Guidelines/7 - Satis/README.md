## Satis - adding composer modules only available for DEPT

At DEPT we don't do open source for our precious code so make sure you add your awesome modules to SATIS.
With Satis we can easily control and update custom composer modules we make.
This guide will help you make your awesome module DEPT exclusive!

This guide will show you how to make, update and manage Satis.
_____________________________________
In bitbucket there is a Satis repo: https://bitbucket.org/tamtam-nl/satis/src/master/
In this repo you will find a satis.json file. In this file are all the modules available to Dept.
In satis.tamtam.nl  you can find all the available modules too.
____________________________________

### Adding a module
Steps:

1. clone the satis repo
2. Go to the satis.json file and add your module in the module list.
add it by using this code (with your info):
```sh
{
  "type": "vcs",
  "url": "https://gitlab.com/dtnl_public/drupal-modules/your_repo_here”
}
```

3. Go to the repo of the module you want to add and add a tag for the initial version.
you can do this by clicking on the tag button and then click on add new tag.
4. Run the manual deploy.sh. type in terminal: ./manual-deploy.sh.
This script will do all the work for you!

NOTE: For the module to be added it must exist of course so make sure all you code is there.

### Update a module
Steps:

1. Go to the module you want to update.
2. Click on ‘’tags’’
3. Click on new tag
4. Increment the value correctly. (Follow semantic versioning)
5. In satis itself ( clone satis if you haven't already ) run: ./manual-deploy.sh
This script will do all the work for you!

### Adding a module to your project
Steps:

1. Add this code to the composer.json file of the project you want to add an exclusive DEPT composer module to:

```
"repositories": [{
    "type": "composer",
    "url": "https://satis.tamtam.nl"
}]
```

2. Then go to https://satis.tamtam.nl/ and copy the name of the module you want to add.
EXAMPLE: template_suggestion_module
3. Just add it like you would with any composer module you want to require.
