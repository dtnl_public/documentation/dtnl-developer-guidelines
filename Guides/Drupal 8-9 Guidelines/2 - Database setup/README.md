# Database setup

Setting up a database for your project is quite easy. There are just
a few stept you need to go trough to set up a database for your PHP project.

For this guide we will be using Sequel Pro for Mac. You can download it here:
https://www.sequelpro.com/



## Installation

1. Open Sequel Pro
2. In the pop-up you get enter the following credentials:
    - Name     : can be whatever you want but I suggest you call it local 
                 just to keep things clean and clear!
    - Host     : we will be developing locally so just enter 127.0.0.1 here
    - Username : set 'root' as username. You can enter whatever name you want
                 but root is the default we use.
    - Password : leave this empty. We are working locally so a password isn't
                 really needed and will just complicate things later.
                 
    All the other fields can be left empty too.
3. In the upper left corner you can see a dropdown which says *Add database*.
   Click on this and then click on the *Add database..* text.
4. Enter a database name *Tip: call it the same as the project your setting up*.
   You can leave the encoding and collation on UTF-8.
   
Now you've created your database! All you have to do is to add this to your
settings.local.php file.

## Getting existing database from server

1. Look up the ssh info on Keeper.
2. login to the server using terminal and the ssh info you got from keeper.
   Make sure you are on the Dept VPN or in a Dept office.
3. The default structure to the project root should alwyas be: /opt/www/site_name/.... but in older projects it can be different.
   Look up the folder structure of an older project [here](https://docs.google.com/spreadsheets/d/1KaVTETAcP5ydjvnLu2GxbUNEIhob4zx5Etov1x3vaRg/edit#gid=0 "sheet").
4. Go to the project root and run: drush sql-dump --result-file=/dump-28-09-2020.sql (enter the correct date)
5. Fire up a fresh terminal screen and make a secure copy of the database file by running: scp USER_NAME@IP_ADDRESS:FOLDER/STRUCTURE/TO_DATABASE/dump-28-09-2020.sql ~/Desktop/databases   
6. You can use the secure copied file and import it into your db

*NOTE:* use -l SITE_NAME if its a multi site setup
