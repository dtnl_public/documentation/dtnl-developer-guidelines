# Swiftmailer

As default mailing system we use Swiftmailer. This module builds on the
standard mail system given by Drupal. 

In Swiftmailer you can set up your mails to be send trough:
- SMTP
- Sendmail
- PHP
- Spool

We recommend using SMTP.
You can request a SMTP server from our DevOps department.

## Setting Swiftmailer up

Most of the setup for sending email is already done so you just have to run
through these steps to start sending mails from your Drupal installation!

1. go to admin/config/swiftmailer/transport
2. Add requested SMTP credentials in here:
      - SMTP server         : the server used to send emails with
      - Port                : Usually 25 but it can be different
      - Encryption          : SSL is what we use by default.
                              Ask DevOps for SSL credentials.
      - Credential Provider : By default we use Swiftmailer
      - Username            : Provided by DevOps (usually apikey)
      - Password            : Provided by DevOps (Sendgrid api key)
3. hit 'save configuration'
4. go to admin/config/swiftmailer/test and test if the email gets send
   *NOTE:* If you're working locally emails send are fetched by mailhog.
           You can check *http://mailhog.valet/* if your mail got send.
   *NOTE:* If you get an error when sendig an email something is wrong
           with your SMTP settings.


## Notes

### *From address*
The from address can be set here: admin/config/people/accounts
By default it uses the system email address. 

-------------------------------

### *Emails for modules*
At admin/config/system/mailsystem?q=/admin/config/system/mailsystem you can 
set which mailer you want to use for specific modules. By default everything is
set to Swiftmailer.

If you add a module that uses email make sure you add that module
in this config and set Swiftmailer as mailing client.



