# Drupal 8+ skeleton

This is the knowledge base for the Drupal 8+ skeleton. 
This knowledge base should provide you with all the information you need
to set up a Drupal 8+ project that meets all the DTNL requirements.

This skeleton contains the standard project structure we use at Dept.
Please keep the project structure the same throughout your project so
other developers can work on the project efficiently.

Also this skeleton has all the modules you need to get started!

If you want to add any modules/code changes/improvements to the skeleton 
contact Lou van Laarse or Marco Koopman.

The Drupal skeleton can be found here: [Drupal skeleton](https://gitlab.com/dtnl_php/project/drupal-8-skeleton)

## Requirements

Before you setup or work on a Drupal 8+ project at Dept make sure you got:


### Git
Is a version control manager we use. You may choose if you want to work with
a terminal or an interface managing your git repositories.

*More info: https://git-scm.com/doc*


### Composer
Is a package manager used by most PHP projects nowadays. 

*More info: https://getcomposer.org/doc/*


### Drush
Is a tool we use to manage our Drupal sites. With crush it’s easy to manage your
modules, configs and even your database.

*More info: https://www.drush.org/*


### Valet+
Is a development environment we use for our local development. With Valet it’s
easy to switch between projects and PHP versions. 

*More info: https://github.com/weprovide/valet-plus/wiki*


### PHP 7+:
Also make sure you’re running the right version of PHP. We use PHP 7+.

### Server access
If you need to make a database export or do something else on a server make sure to check KEEPER for the credentials.

For the folder structure of a project you can check [here](https://docs.google.com/spreadsheets/d/1KaVTETAcP5ydjvnLu2GxbUNEIhob4zx5Etov1x3vaRg/edit#gid=0 "sheet")
