# Azure DevOps

For deployments we use Azure DevOps.
Azure helps us to deploy projects more securely and it holds all information you need regarding you deploys.
Azure DevOps is integrated in every project that we have at DTNL. 
For access to Azure DevOps you need to contact DevOps first and tell them which projects you work on.

Azure DevOps dashboard can be found here: https://dev.azure.com/dept

---------------------
### CI/CD


---------------------
### Pipelines


---------------------
### Releases


---------------------
### Deploy scripts


---------------------
