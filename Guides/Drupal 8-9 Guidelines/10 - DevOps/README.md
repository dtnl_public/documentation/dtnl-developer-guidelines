## Hooks
Letting DevOps handle your deploys is awesome but making your own scripts so DevOps can implement them is even more awesome!
That's why we wrote this little guide. This guide will help you set up your own deploy scripts and communicate these to the DevOps team.

### Why write your own scripts
You may wonder why we write our own deploy scripts especially because DevOps is responsible for these.

Right now it works like this:
In Azure there are a list of steps the deploy process runs through.
These 'steps' are scripts that are being run on the deploy of a project.

Because we (the PHP team) are not implementing these steps ourselves it sometimes happens that these steps will not succesfully deploy a project.
And it is because of this reason we rather write our own deploy script so DevOps can implement this.

This makes the lives of both teams a lot easier!

### How to write your own scripts
If your project is using the DTNL Drupal skeleton you will find a 'hooks' folder in the root of your project.
In this folder you will find a default folder, here you can add your own deploy scripts.

**Note:** If you want to write scripts for a sub site you can add a folder for this site in the hooks folder.
__________________________________
In the deploy script itself you need to describe the steps you want to execute when deploying a project.

**example steps you want to run:**
* drush csim -y
* drush cr

This is basically all you need.

### Letting DevOps know about your scripts
As specified above DevOps will by default add their own steps so you will need to notify them about the custom scripts you wrote.
It's best to do this as soon as possible. They will then add your script to the Azure deploy process.

#### tips
Always write your own scripts.
