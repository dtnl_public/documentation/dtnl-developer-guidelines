# Installation guide

### Step 1
First of you need to make a new folder where you want to keep all your sites in
( if you do not already have one ). Then clone the Drupal 8 Skeleton from:
https://gitlab.com/dtnl_php/project/drupal-8-skeleton

### Step 2
Use Valet+ to link your project so that you can work on it locally.
To do this go to your project’s web folder and run:  valet link NAME_OF_PROJECT

It should give the following message: Current working directory
linked to NAME_OF_PROJECT.valet/
You can use this url now to run your project.

### Step 3
Run composer install in the backend folder (or the folder where the composer.json file is located).
And then run composer update in the same folder.

### Step 4
If you go to the url you just linked your project to you will see a
Drupal 8 installation wizard. You can use this wizard to set up your
Drupal 8 project. But for this you will need a database.
So before you go through the wizard make sure you’ve created an empty database.
A guide on creating a database can be find here: [Database guide](https://gitlab.com/dtnl_php/documentation/dtnl-developer-guidelines/-/blob/master/Guides/Drupal%208%20skeleton/database%20setup/README.md)

The wizard will tell everything you should do.

### Step 5
Copy the example.settings.local.php file and call it settings.local.php

*NOTE:* Please update the example.settings.local.php files accordingly
to your project. This way other developers can set your project up more
easily.

### Step 6
Make sure your local settings file is being used in the settings.php file.
To check this go to the settings.php and look for this code:

```php
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
   include $app_root . '/' . $site_path . '/settings.local.php';
}
```

If its commented out or not in the settings file add it.

Delete this code if it's in the settings.php:

```php
$settings['config_sync_directory'] = 'SOME_CONFIG_DIR';
```

We use a custom directory for this which we set in the local settings.

### Step 7
Copy your database information (this can be found in the settings.php)
and put it in the settings.local.php file.
This way we keep the settings.php as clean as possible.

*IMPORTANT*
- You can delete the DB settings from the settings. 
  php file just to keep things clean.
- You can delete the default.settings.php file if it's created by the
  Drupal installation wizard to install Drupal.
- Delete the 'config_sync_directory' setting from settings.php if it's set.
