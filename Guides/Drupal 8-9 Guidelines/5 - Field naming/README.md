# Field naming convention
In Drupal naming your fields can be quite important especially as your project becomes bigger. Field names tend to 
conflict with each other when these are not named properly. The most common issue here is the delta of a field
the delta is very important because the delta of a field decides what kind of storage mechanism Drupal will use.

The delta of a field is the amount of values that field is allowed or able to store.

Drupal automatically creates a machine name for your field when you put in a name in the 'label'.
It's highly likely that the label you want to show in the dashboard of the user is different than what you want to 
use as machine_name.

### Why do we have naming conventions for our fields?
We're maintaining a strict convention to ensure any developer working on the project. Is able to understand
the project without having to understand the underlying business logic or field structure.

## The naming conventions

In general the naming convention is as follows:

*Base Convention:* `field_{DELTA}_{TITLE}`

If you create a field without a delta value (Delta === 1). Than you will not enter any value for the delta.

*No delta Convention:* `field_{TITLE}`

If you create a field with an unlimited delta. Than you will use N as the value for delta.

*Unlimited Delta Convention:* `field_n_{TITLE}`

## Examples

You want to have a field which can accommodate a single value of the type link field.

Field label name: `Link`
Field machine name: `field_link`

You want to have a field which can accommodate 6 values of the type link field.

Field label name: `Links` <br />
Field machine name: `field_6_links`

You want to have a field which accommodate unlimited values of the link field.

Field label name: `Links` <br />
Field machine name: `field_n_links`

You want to have a field which can accommodate a simple title (values of the type text short):

Field label name: `Title` <br />
Field machine name: `field_title`







