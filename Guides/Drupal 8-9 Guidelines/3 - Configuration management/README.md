# Configuration split

In the skeleton we use the config_split module.
This module can split configurations for each environment.
We use this module because on some environments you might want 
some modules that you don't want on other environments.

*For example:* In the skeleton we use the Xdebug for Twig module.
We only want this module to be active in the local environment
for debugging purposes.

Normally this would be an issue but with config_split this issue is fixed!

## setup

In the skeleton the folder structure and the module are already configured.
the folder structure is as follows: 
- backend
    - config
        - local
        - test
        - acceptance
        - production

In the example.settings.local.php file you can find this code:

```php

$config['config_split.config_split.local']['status'] = TRUE;
$config['config_split.config_split.test']['status'] = FALSE;
$config['config_split.config_split.acceptance']['status'] = FALSE;
$config['config_split.config_split.production']['status'] = FALSE;

```

If you copy this over into your local.setting.php you can manage the configs
for your environments. 

## add a module to a specififc environment
By following these steps you will add a module to a specific environment
and exclude it for all the other environments.

In this example we will be adding a module for the local environment.

1. Go to /admin/config/development/configuration/config-split and click
   on the 'Edit' button next to the 'local config'.
2. Scroll down to the 'COMPLETE SPLIT' block. Here you can see a list 
   of modules and a list of configurations you can 'add' to this environment.
   
   You can do two things here:
   - Add a module to this environment
   - Add a specific config file to this environment
   
   Mostly you will use the first option and just add a whole module to
   an environment. 
   
3. click on the module or config file you want to add to this environment.

   *IMPORTANT:* MAKE SURE YOU HOLD THE COMMAND KEY(mac)/CTRL(windows)
                WHILE CLICKING A MODULE ELSE IT WILL DESELECT
                ALL THE OTHER ADDED MODULES.
                
   *CHECK    :* Double check the .yml file from the environment you're
                adding something to the module and blacklist variables
                are the ones you want to double check if your changes
                are in there. example:
                
                module:
                    toolbar: 0
                    xdebug: 0 
                blacklist: {  }
                
                
   *NOTE     :* adding a module or config will include it for this
                environment BUT exclude it for all the others. 
           
4. click save

Now if you run
```
drush csex 
``` 
it will add the config files of the selected
module and add it in the 'local' folder. 


### setup config files for an environment
1. Set the environment you want to work at on TRUE in the settings.local.php
   *NOTE:* the configs from that environment will be used.
2. Run 
   ``` 
   drush csex 
   ``` 
   to export the configs.
   *NOTE:* if there are any configs to export for this environment
           cex will show these files in the terminal.