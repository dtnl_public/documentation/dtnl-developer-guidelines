# Drupal updates
To keep Drupal safe and secure we need to keep our Drupal installations up to date!
This guide will show you how to update your Drupal 8/9 installation.

### PHP security dashboard
Your first step should be checking the php security dashboard.
In this dashboard you can find Drupal projects that are under a SLA.

This dashboard specifies which Drupal and PHP version our projects currently run.
On each project you can click *Specify* here you can see which modules are being used 
and if they need to be updated.

If a module is marked red: please update it!

You can find the dashboard here: https://php-security.nl.deptagency.com/admin/overview/project

### Update steps
1. Checkout a new branch using your ticket name prefixed with `security/` so if you ticket name is: "DTNL-1337 Drupal update 8.9.10" then your branch name would be: `security/DTNL-1337_Drupal_update_8.9.10`.
2. Go to the security dashboard. Find the project you're trying to update and click on 'Specify'. 
Check which modules are marked red and update these.
Updating modules is like updating the core. Go to composer.json and add the new version of the module there.
IMPORTANT NOTE: ALWAYS CHECK IF THE MODULE IS COMPATIBLE IN THE RELEASE NOTES OF THE MODULE YOU'RE TRYING TO UPDATE.
3. Update the drupal/core. Example:
```
"require": {
  "drupal/core": "8.9.10",
  "drupal/metatag": "1.8",
  "drupal/paragraphs": "1.6",
}
```
4. Run `composer update` to update the modules and commit the files.
5. Run `drush updb -y` locally to ensure the database updates are working. Run `drush csex -y` to ensure updated config gets exported to the yml files.
6. Check your site locally to ensure it works and run `drush cr` to test cache clearing and `drush cron` to test the cron functionality.
7. If working locally, commit your composer.json and composer.lock file.
8. Commit and push your changes. Then merge to develop -> acceptance -> prod