## Paragraphs
Within DTNL options paragraph are often used to provide the webmaster with a "Dynamic" setup.
Which basically entails that the webmaster builds his/her website out of "Blocks" which are re-orderable and re-configurable
by the webmaster. 

## File structure
In general when working with paragraphs you will adhere to the following file structure:

For theme files: <br />
`/themes/dtnl_theme/templates/paragraph/paragraph--{machine_name}` <br />

For modules: <br />
`/modules/custom/paragraphs/paragraph--{machine_name}/paragraph--{machine_name}.module`<br />
`/modules/custom/paragraphs/paragraph--{machine_name}/paragraph--{machine_name}.info`<br />

The reason you create a module for your paragraph is to ensure the code relating to your paragraph is used
in a single location within your Drupal framework. This ensures a clear [separation of concerns](https://en.wikipedia.org/wiki/Separation_of_concerns).

## Preprocess
In general you'll want to use the preprocess function to ensure your data is available under the correct key.
By using the preprocess function to setup your data (which gets passed to a FE component). You're making a clear
[separation of concerns](https://en.wikipedia.org/wiki/Separation_of_concerns) which adheres to the [MVC](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller).


When using preprocess note that early return is easily possible and makes the code a more readable.
We want to keep complexity down as much as possible.

```$php
/**
 * Implements hook_preprocess_HOOK().
 */
function paragraph_header_insurance_detail_preprocess_paragraph(&$variables) {
  $paragraph = $variables["paragraph"];

  if ($paragraph->getType() !== 'header_insurance_detail') {
    return;
  }

  $variables['content']['something'] = 'value';
}
```

If you want to use private methods within the .module
file prefix your method with a underscore and your module name.
```
function _paragraph_header_insurance_my_private_function_name(&$variables) {
```

## Paragraphs Widget

To prevent very long content type edit pages please ensure the Paragraph widget is setup the correct way.

There are 3 types of Paragraph widgets:
* Classic
* EXPERIMENTAL
* Inline Entity form - Complex

![Drag Racing](Images/Paragraph-widget-types.png)

In general you will always want to choose Classic and provide the following configuration:

* *Title:* The title of the function of the widget (generally set as "Block") <br />
* *Plural Title:* The plural title of the function of the widget (generally set as "Blocks")<br />
* *Edit mode:* Closed, unless your reference to the paragraph is equal to 1.<br />
* *Add mode:* Dropdown button<br />
* *Form display mode:* Default<br />
* *Default paragraph type:* None

![Drag Racing](Images/Paragraph-widget-options.png)

## Experimental Paragraph widget
So when do you want to use the experimental paragraph widget?

The experimental paragraph widget is an extension of the classic widget providing more options 
for display, duplicating and collapsing within the admin interface.

Does your client need a duplicate? Upgrade to this widget type.
Does your client want to collapse everything at once? Upgrade to this widget type.