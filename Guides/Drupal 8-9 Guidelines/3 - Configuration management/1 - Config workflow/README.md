## Config management
Config files are the glue that holds Drupal together. 
This glorious glue needs to be maintained very well to make sure you don't get conflicts.
There is nothing worse than importing your configs only to see that you get a big list of errors.

We always use Drush for our configuration management, so this guide is for Drush use only!


### Importing configuration
Importing configuration means your grabbing config files from your local project and push these to your database.
This means that you always need to import configuration files when your pulling a branch or checkout to one.

Doing this will make sure you got the right config files for that branch.
This will also overwrite your current config changes so make sure you don't have any changes you want to keep.

__This is the normal import flow:__
* pull changes or go to target branch
* run `drush csim -y`
* clean cache `drush cr`

Following these steps should give you the config files you need.


### Exporting configuration
Exporting configs means means you're looking in your current linked database for config files and export those to the 
config folder in your local project.

Normally you will export configs when you made changes you want to keep. 

__This is the normal export flow:__
* Make sure you don't already have exported config changes in your branch. You should have a clean branch before beginning.
So before you begin make sure to run a `drush csim -y` and you don't have any lingering config files from previous exports.
* Make the changes you want (in the CMS).
* Run `drush csex -y` to export these changes.
* Commit the config changes you want (always check your config changes when you're adding them to your commit!!).
* Push the commit.
* Go to target branch.
* Pull code
* Run `drush csim -y`
* Check if the config changes are imported correctly.

**ALWAYS CLEAR YOUR CACHE AFTER AND BEFORE RUNNING CSIM/CSEX**