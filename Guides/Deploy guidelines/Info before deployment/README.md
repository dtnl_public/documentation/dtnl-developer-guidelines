# Pre deployment steps / info
Because deploying can make or break a website it's important to take this very seriously.
Azure helps us to manage releases but it cant see when code is broken. 
If all commands run succesfully it will push the code to PROD regarding if the code is broken or not.
So before you deploy your project to PROD it's important to follow this guide!

- Deployments to TEST and ACC are excluded from these rules as the visitors of the website won't see these environments. -

--------------------------------
### Pre deployment steps



--------------------------------

### NOTICE! Important to keep in mind

* always check if your composer.lock file is up to date. This file causes merge conflicts a lot and you might merge an old composer.lock file to the next environment. *SOLUTION:* When composer.lock causes merge conflicts delete it and run composer install (using the master branch of course)
